$(document).ready(function() {
		
    var dlgBody = '';

    // Image button
    $.cleditor.buttons.image = {
        name: 'image',
        title: 'Insert/Upload Image',
        command: 'insertimage',
        popupName: 'image',
        popupClass: 'cleditorPrompt',
        stripIndex: $.cleditor.buttons.image.stripIndex,
        popupContent: 'Please configure imageListUrl',
        buttonClick: insertImageClick,
        uploadUrl: 'imageUpload',
        imageListUrl: 'imagesList',
        popupClick: 'insertImagePopupClick'
    };

    function insertImagePopupClick(event, data)
    {
        console.log("call popup: " +event);
        return true;
    }
    function insertImageClick(e, data) {
//        createDlg($.cleditor.buttons.image.imageListUrl, $.cleditor.buttons.image.uploadUrl, data);

        let editorHTML = '<iframe id="reponsitiveFileManagerFrame" style="width:800px;height:300px;border:0;" src="' + $.cleditor.buttons.image.reponsiveFileManager + '" ></iframe>';
        editorHTML += "<div id='aldusInfo'></div>";
        $(data.popup).html(editorHTML);
        // console.log("e: "+e);
        console.log("ref: " + JSON.stringify($(data.popup).html(), null, 2));
/**
        $.get($.cleditor.buttons.image.reponsiveFileManager, function (dataR) {
            editorHTML = dataR;
            // console.log(editorHTML);
            $(data.popup).html(editorHTML);
        });
*/
    }
	
    function createDlg(imagesListUrl, uploadUrl, data) {
        let editor = data.editor;
        let popup = data.popup;
        let hiddenFrameName = '__uploadIframe';
        let maxThumbsInRow = 6;

        $.getJSON(imagesListUrl, function(jsonData){
            // Create dialog with images list		
            dlgBody = '<div class="tabbable">'+		
            '<ul class="nav nav-tabs">'+
            ' <li class="active"><a href="#tab1" data-toggle="tab">Images list</a></li>'+
            ' <li><a href="#tab2" data-toggle="tab">Upload image</a></li>'+		
            '</ul>'+
            '<div class="tab-content">'+		
            // Images list panel
            ' <div class="tab-pane active span7" id="tab1">'+
            '  <p>Images list</p>'+
            '  <table style="overflow: auto;" class="well"><tr>';
            let cc = 0;
            for ( var i = 0; i < jsonData.length; i++) {			
                dlgBody += '<td style="padding: 5px;">'+
                '<a href="#" id="imgBtn'+i+'" class="btn thumbnail">'+					
                '<img src="'+jsonData[i]+'" width="80"/>'+
                '</a></td>';
                if (++cc % maxThumbsInRow == 0){
                    dlgBody += "</tr><tr>";
                }
            }

            for (var i = (cc % maxThumbsInRow); i < maxThumbsInRow; i++) {
                dlgBody += "<td></td>";
            }
            dlgBody += '</tr></table>'+
                '<p>' + $.cleditor.buttons.image.reponsiveFileManager + '</p>'+
            '</div>'+

            // Image upload panel
        
            '<div class="tab-pane span7" id="tab2">'+
            '<iframe style="width:0;height:0;border:0;" name="' + hiddenFrameName + '" ></iframe>' +
            '<table class="cleditorPopupInsertImage">' +
            '<tr><td>Choose a File:</td></tr>' +
            '<tr><td> ' +
            '<form id="imgUploadForm" method="post" enctype="multipart/form-data" action="" target="' + hiddenFrameName + '">' +
            '<input id="imageName" name="imageName" type="file" /></form> </td></tr>' +
            '<tr><td>Or enter URL:</td></tr>' +
            '<tr><td><input type="text" size="40" value="" /></td></tr>' +
            '</table><input type="button" id="uploadBtn" value="Submit">'+		
            '</div>'+	
            '</div>'+			
            '</div>';       
            $(popup).html(dlgBody);

            // Insert image
            $(data.popup).find('.btn').unbind("click").bind("click", function(e) {
                e.preventDefault();
                // Insert some image into the document
                let tempHTML = $(this).children("img").attr("src");

                // console.log("data: "+data.command);
                editor.execCommand(data.command, tempHTML, null, data.button);
                hidePopup(editor);
            });

            let urlTextField = $(data.popup).find(':text'),
                url = $.trim(urlTextField.val()),
                uploadIframe = $(data.popup).find('iframe'),
                loadedFile = $(data.popup).find(':file');

            // Cleaning of previously selected file and url
            loadedFile.val('');
            urlTextField.val('').focus();

            // Submit button click event		
            $(data.popup).find(':button').unbind("click").bind("click", function(e) {
                // User downloads the file
                if (loadedFile.val()) {
                    uploadIframe.bind('load', function() {
                        var fileUrl = '';
                        try {
                            fileUrl = uploadIframe.get(0).contentWindow.document.getElementById('image').innerHTML;
                        } catch(e) {
                            console.log('Error: ' + e);
                        }                        
                        if(fileUrl) {
                            editor.execCommand(data.command, fileUrl, null, data.button);
                        } else {
                            alert('An error occurred during upload!');
                        }
                        uploadIframe.unbind('load');
                        hidePopup(editor);
                    });
                    $(data.popup).find('form').attr('action', uploadUrl);
                    $(data.popup).find('form').submit();
                // User puts URL
                } else if (url != '') {
                    editor.execCommand(data.command, url, null, data.button);
                    hidePopup(editor);
                }
            });

        });
    }		       
   
    function hidePopup(editor) {
        editor.hidePopups();
        editor.focus();		
    }

    function responsive_filemanager_callback(field_id){
        console.log(field_id);
        var url=jQuery('#'+field_id).val();
        alert('update '+field_id+" with "+url);
        //your code
    }

    window.addEventListener('message', function receiveMessage(event) {
        // window.removeEventListener('message', receiveMessage, false);
        if (event.data.sender === 'responsivefilemanager') {
            // callback(event.data.url);
            console.log("call: " + event.data.url);
            // console.log("editor: " + (event.data));
            // console.log(JSON.stringify(window.$.cleditor, null, 2));
            // console.log("ref: " + JSON.stringify(window.$('textarea').cleditor()[0].$area, null, 2));
            // window.$.cleditor.buttons.image.responsive_filemanager_callback(event.data.url);
            // window.$.cleditor.execCommand('insertimage', event.data.url, null, $.cleditor.buttons.image);
            // window.$.cleditor.hidePopups();
            // window.$.cleditor.focus();
            let info = document.getElementById("aldusInfo");
            info.innerHTML = event.data.url;
            let ref = document.getElementById("reponsitiveFileManagerFrame");
            if (ref)
            {
                console.log("ref: " + ref);
            }
        }
    }, {passive:true});
});