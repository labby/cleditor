<?php

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

header('Content-Type: application/javascript');

// $database = LEPTON_database::getInstance();

LEPTON_handle::register("file_list");
$allFiles = file_list(
    LEPTON_PATH.MEDIA_DIRECTORY,
    [],
    false,
    "jpg|png",
    LEPTON_PATH,
    true
);
$allFiles = array_map(function ($a){return LEPTON_URL.$a;}, $allFiles);

$allFiles = array_merge($allFiles, []);
// echo LEPTON_tools::display($allFiles, "pre");
echo json_encode($allFiles);
