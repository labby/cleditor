<?php

/**
 *  @module         CLEditor
 *  @version        see info.php of this module
 *  @authors        erpe, Aldus
 *  @copyright      2023-2023 erpe, Aldus
 *  @license        MIT  License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

$image_url = "";
// $po = LEPTON_tools::display($_FILES);
if (isset($_FILES['imageName']) && (!empty($_FILES['imageName'])))
{
    $ziel = LEPTON_PATH.MEDIA_DIRECTORY."/test/";
    $filename = $ziel.$_FILES['imageName']['name'];
    move_uploaded_file($_FILES['imageName']['tmp_name'], $ziel.$_FILES['imageName']['name']);

    $image_url = str_replace(LEPTON_PATH, LEPTON_URL, $filename);
}
?>
<div id="image"><?= $image_url ?></div>