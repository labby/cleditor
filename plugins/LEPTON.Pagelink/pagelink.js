// https://premiumsoftware.net/cleditor/plugins

(function($) {

    // Define the pagelink button
    $.cleditor.buttons.pagelink = {
        name: "pagelink",
        image: "pagelink.gif",
        title: "Lepton - internal pagelink",
        command: "inserthtml",
        popupName: "pagelink",
        popupClass: "cleditorPrompt",
        popupContent: "<h5>Pagelink</h5>"+ $.cleditor.defaultOptions.LEPTON_PAGE_SELECT +"<br><br><input type=button value="+$.cleditor.defaultOptions.LEPTON_PAGELINK_LANG.submit+">",
        buttonClick: pagelinkClick
    };

    // Add the button to the default controls before the bold button
    $.cleditor.defaultOptions.controls = $.cleditor.defaultOptions.controls
        .replace("bold", "pagelink bold");

    // Handle the hello button click event
    function pagelinkClick(e, data) {

        // Wire up the submit button click event
        $(data.popup).children(":button")
            .unbind("click")
            .bind("click", function (e) {

                // Get the editor
                let editor = data.editor;

                let linkID = $(data.popup).find("#LEPTON_pagelink").val();
                let linkText = $(data.popup).find("#LEPTON_pagelink :selected").text();
                linkText = linkText.replaceAll("- ", "");

                // Any selected text?
                let selectedText = editor.selectedText();
                if (selectedText != "")
                {
                    linkText = selectedText;
                }

                let html = "<a href='[LEPTONlink" + linkID + "]'>" + linkText + "</a>";
                editor.execCommand(data.command, html, null, data.button);

                //
                console.log('link: ' + $.cleditor.buttons.image.imageListUrl );

                // Hide the popup and set focus back to the editor
                editor.hidePopups();
                editor.focus();

            });

    }
})(jQuery);
