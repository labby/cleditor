<?php

/**
 *  @module         CLEditor
 *  @version        see info.php of this module
 *  @authors        erpe, Aldus
 *  @copyright      2023-2023 erpe, Aldus
 *  @license        MIT  License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 *
 */

$files_to_register = [
    "pagelink_init.php",
    "pagelink_test.php",
    "pagelink.php"
];

LEPTON_secure::getInstance()->accessFiles($files_to_register);
