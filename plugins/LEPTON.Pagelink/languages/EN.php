<?php

$MOD_CLEDITOR_PAGELINK = [
    "title" => "LEPTON internal paggelink",
    "tooltip" => "Insert a LEPTON page",
    "label_select" => "Select a page to insert ...",
    "submit"    => "Insert pagelink"
];
