<?php

/**
 *  @module         CLEditor
 *  @version        see info.php of this module
 *  @authors        erpe, Aldus
 *  @copyright      2023-2023 erpe, Aldus
 *  @license        MIT  License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

// -- [1]
header('Content-Type: application/javascript');

// -- [2]
function buildPageSelectOptions(array $pages, int $deep = 0, string &$storage=""): void
{
    foreach ($pages as $tempPage)
    {
        $menuTitle = str_repeat("- ", $deep).$tempPage['menu_title'];

        $storage .= "<option value='".$tempPage['page_id']."'>".$menuTitle."</option>\n";

        if (!empty($tempPage['subpages']))
        {
            buildPageSelectOptions($tempPage['subpages'], $deep + 1, $storage);
        }
    }
}

$database = LEPTON_database::getInstance();
LEPTON_handle::register("page_tree");

$allPages = [];
page_tree( 0, $allPages );

global $MOD_CLEDITOR_PAGELINK;
require __DIR__."/languages/EN.php";

$myHTML = "<p></p>".$MOD_CLEDITOR_PAGELINK['label_select']."<br><select id='LEPTON_pagelink' style='min-width: 300px;'>\n";

buildPageSelectOptions($allPages, 0, $myHTML);

$myHTML .= "\n</select>\n";

$returnValue = "
    $.cleditor.defaultOptions.LEPTON_PAGE_SELECT = ".json_encode($myHTML).";
    $.cleditor.defaultOptions.LEPTON_PAGELINK_LANG = ".json_encode($MOD_CLEDITOR_PAGELINK).";
";

echo $returnValue;