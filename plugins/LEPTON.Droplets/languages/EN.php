<?php

$MOD_CLEDITOR_DROPLETS = [
    "title" => "LEPTON Droplets",
    "tooltip" => "Insert a LEPTON droplet",
    "label_select" => "Select a droplet to insert ...",
    "submit"    => "Insert ..."
];
