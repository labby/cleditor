<?php

/**
 *  @module         CLEditor
 *  @version        see info.php of this module
 *  @authors        erpe, Aldus
 *  @copyright      2023-2023 erpe, Aldus
 *  @license        MIT  License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

header('Content-Type: application/javascript');

$database = LEPTON_database::getInstance();
$allDroplets = [];
$database->execute_query(
    "SELECT name, id, comments FROM ".TABLE_PREFIX."mod_droplets WHERE active=1 ORDER BY name",
    true,
    $allDroplets,
    true
);

global $MOD_CLEDITOR_DROPLETS;
require __DIR__."/languages/EN.php";

$js = "<script>
function displayDropletInfo(ref) {
    let index = ref.selectedIndex;
    let info = \$.cleditor.defaultOptions.LEPTON_DROPLETS_COMMENTS[index];
    document.getElementById('LEPTON_droplets_infobox').innerHTML = '<p style=\'margin-bottom:10px;\'>Description:</p>'+info;
    // console.log('tt: ' + \$('#LEPTON_STORAGE').data('LEPTON_DROPLETS_COMMENTS'));
}
</script>";

$myHTML = $js."<p></p>".$MOD_CLEDITOR_DROPLETS['label_select']."<br><select onchange='displayDropletInfo(this);' id='LEPTON_droplets' style='min-width: 300px;'>\n";



$comments = [];
foreach ($allDroplets as $dropletInfo)
{
    $myHTML .= "\n<option value='".$dropletInfo['name']."'>".$dropletInfo['name']."</option>";
    $comments[] = $dropletInfo['comments'];
}
$myHTML .= "</select><p></p>\n";

$myHTML .= "<p>Optional Params:<br><input type='text' id='LEPTON_droplets_params' style='border-width: 1px;'/></p>";

$myHTML .= "<div id='LEPTON_droplets_infobox' style='display:block;min-width:300px; max-width:300px; height:90px'></div>";

$returnValue = "
    $.cleditor.defaultOptions.LEPTON_DROPLETS =  ".json_encode($myHTML).";
    $.cleditor.defaultOptions.LEPTON_DROPLETS_LANG = ".json_encode($MOD_CLEDITOR_DROPLETS).";
    $.cleditor.defaultOptions.LEPTON_DROPLETS_COMMENTS = ".json_encode($comments).";
";

echo $returnValue;