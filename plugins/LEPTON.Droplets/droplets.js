// https://premiumsoftware.net/cleditor/plugins

(function($) {

    // Define the droplets button
    $.cleditor.buttons.droplets = {
        name: "droplets",
        image: "droplets.gif", // "hello.gif",
        width: "399",
        title: "Lepton - droplets",
        command: "inserthtml",
        popupName: "droplets",
        popupClass: "cleditorPrompt",
        popupContent: "<h5>Droplet</h5>"+ $.cleditor.defaultOptions.LEPTON_DROPLETS +"<br><input type=button value='"+$.cleditor.defaultOptions.LEPTON_DROPLETS_LANG.submit+"'>",
        buttonClick: dropletsClick
    };

    // Add the button to the default controls before the bold button
    $.cleditor.defaultOptions.controls = $.cleditor.defaultOptions.controls
        .replace("bold", "hello bold");

    // Handle the hello button click event
    function dropletsClick(e, data) {

        // Wire up the submit button click event
        $(data.popup).children(":button")
            .unbind("click")
            .bind("click", function (e) {

                // Get the editor
                let editor = data.editor;

                let droplet = $(data.popup).find("#LEPTON_droplets").val();

                // any params?
                let params = $(data.popup).find("#LEPTON_droplets_params").val();
                if (params != ""){
                    params = params.replace("?", "");
                    params = "?"+params;
                }

                let html = "[[" + droplet + params + "]]";
                editor.execCommand(data.command, html, null, data.button);

                // Hide the popup and set focus back to the editor
                editor.hidePopups();
                editor.focus();

            });

    }
})(jQuery);
