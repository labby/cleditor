<?php

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

header('Content-Type: application/javascript');

$database = LEPTON_database::getInstance();
$allDroplets = [];
$database->execute_query(
    "SELECT name, id FROM ".TABLE_PREFIX."mod_droplets ORDER BY name",
    true,
    $allDroplets,
    true
);

global $MOD_CLEDITOR_DROPLETS;
require __DIR__."/languages/EN.php";

$myHTML = "<p></p>".$MOD_CLEDITOR_DROPLETS['label_select']."<br><select id='LEPTON_droplets' style='min-width: 300px;'>\n";

foreach ($allDroplets as $dropletInfo)
{
    $myHTML .= "\n<option value='".$dropletInfo['name']."'>".$dropletInfo['name']."</option>";
}
$myHTML .= "</select><p></p>\n";



$returnValue = "
    var LEPTON_URL='".LEPTON_URL."';
    console.log('aldus: ' + LEPTON_URL);
    $('#LEPTON_STORAGE').data('LEPTON_DATA', LEPTON_URL);
    $('#LEPTON_STORAGE').data('LEPTON_DROPLETS', ".json_encode($myHTML).");
    $('#LEPTON_STORAGE').data('LEPTON_DROPLETS_LANG', ".json_encode($MOD_CLEDITOR_DROPLETS).");
";

echo $returnValue;