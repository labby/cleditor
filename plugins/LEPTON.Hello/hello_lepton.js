// https://premiumsoftware.net/cleditor/plugins
// console.log($("#LEPTON_STORAGE").data("LEPTON_DROPLETS_LANG").tooltip);
(function($) {

    // Define the hello button
    $.cleditor.buttons.hello = {
        name: "hello",
        image: "droplets.gif", // "hello.gif",
        width: "399",
        title: "Hello Lepton - droplets",
        command: "inserthtml",
        popupName: "hello",
        popupClass: "cleditorPrompt",
        popupContent: "<h5>Droplet</h5>"+ $('#LEPTON_STORAGE').data('LEPTON_DROPLETS') +"<br><input type=button value="+$("#LEPTON_STORAGE").data("LEPTON_DROPLETS_LANG").submit+">",
        buttonClick: helloClick
    };

    // Add the button to the default controls before the bold button
    $.cleditor.defaultOptions.controls = $.cleditor.defaultOptions.controls
        .replace("bold", "hello bold");

    // Handle the hello button click event
    function helloClick(e, data) {

        // Wire up the submit button click event
        $(data.popup).children(":button")
            .unbind("click")
            .bind("click", function (e) {

                // Get the editor
                var editor = data.editor;

                var droplet = $(data.popup).find("#LEPTON_droplets").val();
                //console.log("droplet: " + droplet);

                var html = "[[" + droplet + "]]";
                editor.execCommand(data.command, html, null, data.button);

                // Hide the popup and set focus back to the editor
                editor.hidePopups();
                editor.focus();

            });

    }
})(jQuery);
