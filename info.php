<?php

/**
 *  @module         CLEditor
 *  @version        see info.php of this module
 *  @authors        erpe, Aldus
 *  @copyright      2023-2023 erpe, Aldus
 *  @license        MIT  License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$module_directory     = 'cleditor';
$module_name          = 'CLEditor';
$module_function      = 'WYSIWYG';
$module_version       = '1.4.5.1';
$module_platform      = '6.x';
$module_author        = 'erpe, Aldus';
$module_home          = 'https://lepton-cms.org';
$module_guid          = '120bf8a6-7c83-4499-b794-2c5ecbf947f5';
$module_license       = 'GNU General Public License, CLEditor is MIT';
$module_license_terms  = '-';
$module_description   = '<a href="https://premiumsoftware.net/cleditor/" target="_blank">Current CLEditor </a>allows you to edit the content<br />of a page and see media image folder.';
