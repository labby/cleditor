<?php

declare(strict_types=1);

/**
 *  @module         CLEditor
 *  @version        see info.php of this module
 *  @authors        erpe, Aldus
 *  @copyright      2023-2023 erpe, Aldus
 *  @license        MIT  License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 *
 *
 */
 
class cleditor_settings extends LEPTON_abstract implements LEPTON_wysiwyg_interface
{
	public string $default_skin= "cleditor";
	public string $default_height = "500";
	public string $default_width = "960";
	public string $default_toolbar = "Full";
	public string $default_content_css = "document";

    public string $content = "This text comes from the CLEditor-class as a property.\n";

	public array $skins = [];
	public array $content_css = [];
	public array $toolbars = [];
	
	public static $instance;
	public function initialize() 
	{
		$ignore_files = ["index.php"];

		$this->skins = [ $this->default_skin ];

		$this->toolbars = [
				
			'Full'	=> "icon | table | droplets | pagelink | bold italic underline strikethrough subscript superscript | font size " .
                "style | color highlight removeformat | bullets numbering | outdent " .
                "indent | alignleft center alignright justify | undo redo | " .
                "rule image link unlink | cut copy paste pastetext | print source",

			/**
			 *	Smart toolbar within only first two rows.
			 *
			 */
			'Smart' => "icon | table | droplets | pagelink | bold italic underline strikethrough subscript superscript | font size " .
                "style | color highlight removeformat | bullets numbering | outdent | undo redo | image link unlink | source",


            /**
			 *	Simple toolbar within only one row.
			 *
			 */
			'Simple' => "droplets | pagelink | bold italic underline strikethrough | font size " .
                "removeformat | bullets numbering | alignleft center alignright justify | undo redo | image source"


        ];
	}
        
        // interfaces
        //  [1]
        public function getHeight(): string
        {
           return $this->default_height; 
        }
        //  [2]
        public function getWidth(): string
        {
            return $this->default_width; 
        }
        //  [3]
        public function getToolbar(): array
        {
            return $this->toolbars;
        }
        //  [4]
        public function getSkin(): string
        {
            return $this->default_skin ?? ""; 
        }
}	

