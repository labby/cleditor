<?php

declare(strict_types=1);

/**
 *  @module         CLEditor
 *  @version        see info.php of this module
 *  @authors        erpe, Aldus
 *  @copyright      2023-2023 erpe, Aldus
 *  @license        MIT  License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 * @see: https://premiumsoftware.net/cleditor/gettingstarted
 *
 */

class cleditor extends LEPTON_abstract 
{
    static bool $editorLoaded = false;

    static private string $rfmAccessKey = "Just123A7o8";

    /**
    * Own instance of the class
    * @var object|instance
    */
    static $instance;

    public string $selector = "";
    /**
    * Required by LEPTON_abstract - called after "getInstance()"
    */
    public function initialize() 
    {
        // Own initializations here:
        $this->initEditor();
    }

    /**
     * Initialize editor and create an textarea
     *
     * @param string $name Name of the textarea.
     * @param string $id Id of the textarea.
     * @param string $content The content to edit.
     * @param int|string|null $width The width of the editor, overwritten by wysiwyg-admin.
     * @param int|string|null $height The height of the editor, overwritten by wysiwyg-admin.
     * @param bool $prompt Direct output to the client via echo (true) or returned as HTML-textarea (false)?
     * @return bool|string  Could be a BOOL or STR (textarea-tags).
     *
     */
	static public function show_wysiwyg_editor(string $name, string $id, string $content, int|string|null $width=null, int|string|null $height=null, bool $prompt=true): bool|string
	{
        $database = LEPTON_database::getInstance();

        $self = self::getInstance();

        $oEditorSettings = cleditor_settings::getInstance();

        $wysiwygToolbar = $database->get_one("SELECT toolbar from `".TABLE_PREFIX."mod_wysiwyg_settings` WHERE editor = 'cleditor'");
        if ($wysiwygToolbar != null)
        {
            $controls = $oEditorSettings->getToolbar()[$wysiwygToolbar];
        } else {
            $controls = $oEditorSettings->getToolbar()[$oEditorSettings->default_toolbar];
        }
        $_SESSION['rfkey'] = self::$rfmAccessKey;
        // Get Twig
        $oTWIG = lib_twig_box::getInstance();
        $oTWIG->registerModule('cleditor');

        $data = [
            'id' => $id,
            'name' => $name,
            'content' => $content,
            'width' => $width ?? "100%",
            'height' => $height ?? "250px",
            'TEMPLATE' => self::getTemplate(),
            'controls' => $controls
        ];

        $result = $oTWIG->render(
            "@cleditor/quick.lte",    //  template-filename
            $data            //  template-data
        );

        if ($prompt === true)
        {
            echo $result;
            return "";
        } else {
            return $result;
        }
	}

    /**
     * @return void
     */
    protected function initEditor(): void
    {
        LEPTON_handle::register("random_string");
        self::$rfmAccessKey = random_string(16);

        $oTWIG = lib_twig_box::getInstance();
        $oTWIG->registerModule('cleditor');

        echo $oTWIG->render(
            "@cleditor/cleditor.lte",
            [
                'ACCESS_KEY' => self::$rfmAccessKey
            ]
        );
    }

    /**
     * @return string
     */
    static public function getTemplate(): string
    {
        $returnValue = defined("TEMPLATE") ? TEMPLATE : DEFAULT_TEMPLATE;

        if (defined('PAGE_ID'))
        {
            $tempTemplate = LEPTON_database::getInstance()->get_one(
                "SELECT template FROM ".TABLE_PREFIX."pages WHERE page_id=".PAGE_ID
            );
            if ($tempTemplate != null)
            {
                $returnValue = $tempTemplate;
            }
        }

        return $returnValue;
    }
}
