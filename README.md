# CLEditor
This svn includes CLEditor in [LEPTON CMS][1] as an addon. 

#### Based on
- https://premiumsoftware.net/cleditor/
- https://premiumsoftware.net/cleditor/gettingstarted
- https://github.com/cleditor/cleditor
- https://github.com/alex-bezverkhniy/cleditor-imageupload-plugin

#### Requirements
* PHP 8.0 recommended
* [LEPTON CMS][1] (6.0 recommended)

#### Quick test
- Code2 section; type: PHP

``` php
$oCL = cleditor::getInstance();
echo cleditor::show_wysiwyg_editor(
  "testTextArea",
  "id1234567",
  "[[lorem2?set=berthold&max=150]]",
  "50%",
  "300px",
  false
);
```

---
[1]: http://lepton-cms.org "LEPTON CMS"